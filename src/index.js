import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

window.renderAppTwo = containerId => {
  ReactDOM.render(<App />, document.getElementById(containerId));
}

window.unmountAppTwo = containerId => {
  ReactDOM.unmountComponentAtNode(document.getElementById(containerId));
}

if(!document.getElementById("AppTwo-container")) {
  ReactDOM.render(<App />, document.getElementById("root"));
}
