import './App.css';
import { useState } from 'react';

function App() {

  const [name, setname] = useState("")

  const handleClick = () =>{
    setname("welcome to Application Two")
  }



  return (
    <div className="App">
      <h2>Application two</h2>
      <button className="btn" onClick={handleClick}>Click here</button>
      {
        name && <h1>{name}</h1>
      }
    </div>
  );
}

export default App;
